# Was ist Git?

---

## Lernziel

Ein Überblick in die Git-Landschaft

 * Wie ist Git entstanden?
 * Welche Eigenschaften zeichnen es aus?
 * Warum ist es so verbreitet?
 * Git-Kommandozeile, User-Interfaces und Git-Hoster
 * Wissen, wo man mehr Informationen findet.

